bl_info = {
    "name": "Shape View",
    "author": "Joseph Eagar",
    "version": (0, 1),
    "blender": (2, 80, 0),
    "location": "Properties > Object Data > ShapeView",
    "description": "Activate shapekeys based on camera viewpoint",
    "warning": "",
    "wiki_url": "",
    "category": "Rigging",
    }

import bpy
from . import Global, ops, props, ui, utils, shapeview, select_area_op

modules = [
  ops,
  props,
  ui,
  select_area_op,
]

def render_pre(a, b):
  Global.is_rendering = True

def render_post(a, b):
  Global.is_rendering = False

def register():
  for m in modules:
    if hasattr(m, 'register'):
      m.register()
    elif hasattr(m, 'classes'):
      for c in m.classes:
        bpy.utils.register_class(c)
    else:
      print("Can't register module")
      print(m)

def unregister():
  for m in modules:
    if hasattr(m, 'unregister'):
      m.unregister()
    elif hasattr(m, 'classes'):
      for c in m.classes:
        bpy.utils.unregister_class(c)
